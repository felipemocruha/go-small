simple:
	go build -o simple

static:
	CGO_ENABLED=0 go build -o static

static_with_flags:
	CGO_ENABLED=0 go build -o static_with_flags -ldflags="-w -s"

static_upx:
	CGO_ENABLED=0 go build -o static_upx -ldflags="-w -s"
	upx --brute static_upx

docker:
	docker build -t registry.gitlab.com/felipemocruha/go-small:v0.1.0 -f Dockerfile .
