module gitlab.com/felipemocruha/go-small

go 1.13

require (
	github.com/jwilder/docker-squash v0.2.0 // indirect
	github.com/labstack/echo/v4 v4.1.16
)
