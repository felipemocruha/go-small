FROM gcr.io/distroless/static

COPY static_upx /

ENTRYPOINT ["/static_upx"]

